<?php

namespace App\Controller;

use DateTime;
use App\Entity\Room;
use App\Form\RoomType;
use DateTimeInterface;
use App\Data\SearchData;
use App\Form\SearchType;
use App\Repository\RoomRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class RoomController extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }


    /**
     * @Route("/room", name="room")
     */
    public function index(RoomRepository $roomRepository, Request $request): Response
    {

        // $rooms = $roomRepository->findAvailableRoom();
        $data = new SearchData();
        $data->page = $request->get('page',1);
        $form = $this->createForm(SearchType::class,$data);
        $form->handleRequest($request);

        $rooms = $roomRepository->findQueryResult($data);

        if ($form->get('clear')->isClicked()) {
            return $this->redirectToRoute('room');
        }

        return $this->render('room/index.html.twig', [
            'controller_name' => 'RoomController',
            'rooms'=>$rooms,
            'form'=>$form->createView()
        ]);
    }
  /**
     * @Route("/new", name="room.add", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */

    public function new(Request $request) : Response
    {
        $dateImmutable = \DateTime::createFromFormat('Y-m-d H:i:s', strtotime('now')); # also tried using \DateTimeImmutable

        $room = new Room();
        $room->setUser($this->security->getUser());
        $room->setCreatedAt(new DateTime());
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager =$this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();


            $this->addFlash('success', 'la salle a bine etait enregistrer');
            return $this->redirectToRoute('room');
        }
        return $this->render('room/new.html.twig',[
            'room'=>$room,
            'form'=>$form->createView()
        ]);
    }


/**
     * @Route("/{id}/edit",name="room.edit", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Room $room) : Response
    {       
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $room->setUpdatedAt(new DateTime());
            // dd($room);
            $entityManager = $this->getDoctrine()->getManager();
            // dd($entityManager);
            // $entityManager->persist($room);
            $entityManager->flush();
            $this->addFlash('success','La salle a bien été modifée');
            return $this->redirectToRoute('room');
        }
        return $this->render('room/edit.html.twig',[
            'room'=>$room,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/{id}",name="room.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Room $room) : Response
    {
        if($this->isCsrfTokenValid('delete'.$room->getId(),$request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();

            $this->addFlash('success','La salle a bien été supprimée');
            return $this->redirectToRoute('room');
        }
    }


    /**
     * @Route("/room/{city}", name="room.showCity")
     * @return Response
     */
    public function showRoomsByCity(RoomRepository $repository,$city): Response
    {

        $rooms = $repository->findByCity($city);

        // dd($rooms);

        return $this->render('room/showCity.html.twig', [
            'rooms'=>$rooms
        ]);
    }

}
