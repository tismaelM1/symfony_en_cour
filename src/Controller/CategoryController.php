<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index(CategoryRepository $CategoryRepo): Response
    {
        $categories = $CategoryRepo->findAvailableCategory();

        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'categories'=>$categories

        ]);
    }
    


    /**
     * @Route("/category/add", name="category.add")
     */
    public function add(Request $request): Response
    {
        $categorie = new Category();

        $form = $this->createForm(CategoryType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager =$this->getDoctrine()->getManager();
            $entityManager->persist($categorie);
            $entityManager->flush();

            $this->addFlash('success', 'la categories a bine etait enregistrer');
            return $this->redirectToRoute('category');
        }


        return $this->render('category/add.html.twig', [
            'controller_name' => 'CategoryController',
            'form'=>$form->createView()

        ]);
    }


    /**
     * @Route("/category/{id}/edit", name="category.edit")
     */
    public function edit(Request $request,Category $categorie): Response
    {
        $form = $this->createForm(CategoryType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager =$this->getDoctrine()->getManager();
            // $entityManager->persist($categorie);
            $entityManager->flush();

            $this->addFlash('success', 'la categories a bine etait enregistrer');
            return $this->redirectToRoute('category');
        }


        return $this->render('category/edit.html.twig', [
            'form'=>$form->createView(),
            'categorie'=>$categorie,


        ]);
    }

        /**
     * @Route("/{id}/category",name="category.delete", methods={"DELETE"})
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function delete(Request $request, Category $categorie) : Response
    {
        if($this->isCsrfTokenValid('delete'.$categorie->getId(),$request->request->get('_token'))){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorie);
            $entityManager->flush();

            $this->addFlash('success','La categorie a bien été supprimée');
            return $this->redirectToRoute('category');
        }
    }
}
