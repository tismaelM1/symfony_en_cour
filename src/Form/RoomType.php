<?php

namespace App\Form;

use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\Length;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class,[
                'required'=>false,
                'label'=>false,
                'attr'=>[
                    'placeholder'=>'nom de la salle',
                    'class'=>'bg-info'
                ],
            ])
            ->add('capacity',IntegerType::class)
            ->add('city',TextType::class)
            ->add('description', TextareaType::class, [
                'required'=>false,
                'constraints'=>[
                    new NotBlank([
                        'message' => 'Merci d\'ajouter une description'
                    ]),
                    new Length([
                        'min'=>10,
                        'max'=>600,
                        'minMessage'=>'Le message doit faire au moin 10 caractere ',
                        'maxMessage'=>'Le message ne dois pas ecceder 600 car'

                    ])
                ]
            ])
            ->add('categories')


            // ->add('isAvailable')
            // ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
            'translation_domain'=>'room_form'
        ]);
    }
}
