<?php

namespace App\Form;

use App\data\SearchData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('qName',TextType::class ,[
                'required'=>false,
                'label'=>false,
                'attr'=>[
                    'placeholder'=> 'Search by name'
                ]
            ])
            ->add('qCity',TextType::class,[
                'required'=>false,
                'label'=>false,
                'attr'=>[
                    'placeholder'=> 'Search by city'
                ]
            ])
            ->add('clear',SubmitType::class,[
                'label'=>"reset search",
                'attr'=>[
                    'class'=>'mt-2 btn btn-danger w-100'
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'=>SearchData::class,
            'method'=>'GET',
            'csrf_protection'=>false
            // Configure your form options here
        ]);
    }
}
