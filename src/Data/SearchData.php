<?php 

namespace App\Data;

class SearchData
{
    /**
     * @var string
     */
    public $qName;
    /**
     * @var string
     */
    public $qCity;

    /**
     * @var int
     */
    public $page =1;
}